﻿using System;
using System.Collections.Generic;
using System.Reflection;
using ConsoleApplications.Common.Extensions;
using ConsoleApplications.Common.Interfaces;
using DNX.Helpers.Assemblies;
using DNX.Helpers.Strings.Interpolation;

namespace ConsoleApplications.Common
{
    public class AppHeader : IAppHeader
    {
        private readonly List<string> _outputLines = new();

        public AppHeader()
            : this(Assembly.GetEntryAssembly())
        {
        }

        public AppHeader(Assembly assembly)
        {
            var assemblyDetails = assembly.GetAssemblyDetails();

            var assemblyYear = assembly.GetLinkerTime().Year;

            var details = new
            {
                year = DateTime.UtcNow.Year > assemblyYear
                    ? $"{assemblyYear}-{DateTime.UtcNow.Year}"
                    : assemblyYear.ToString()
            };

            _outputLines.Add($"{assemblyDetails.Name} v{assemblyDetails.Version.Simplify()} - {assemblyDetails.Title}");
            _outputLines.Add($"{assemblyDetails.Copyright.InterpolateWith(details)}");

            _outputLines.RemoveAll(string.IsNullOrEmpty);
        }

        public string[] GetOutputLines() => _outputLines.ToArray();
    }
}
