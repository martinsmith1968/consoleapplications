﻿namespace ConsoleApplications.Common.Interfaces
{
    public interface IAppHeader
    {
        string[] GetOutputLines();
    }
}
