﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ConsoleApplications.Common;
using ConsoleApplications.Common.Interfaces;
using Ookii.CommandLine;

namespace PathList
{
    internal class Program
    {
        private static async Task<int> Main(string[] args)
        {
            var parser = new CommandLineParser(typeof(Arguments), new[] { "-", "/" })
            {
                AllowDuplicateArguments       = false,
                AllowWhiteSpaceValueSeparator = true,
            };

            var options = new WriteUsageOptions()
            {
                IncludeAliasInDescription        = true,
                IncludeApplicationDescription    = true,
                IncludeDefaultValueInDescription = false,
                Indent                           = 2,
            };

            try
            {
                ShowAppHeader(new AppHeader());

                var arguments = (Arguments)parser.Parse(args);

                var pathText = GetEnvironmentVariableValue(arguments);

                var paths = SplitPaths(pathText);

                var pathDetails = await BuildPathDetails(paths);

                EnrichPathDetails(pathDetails);

                var indexWidth = pathDetails.Count.ToString().Length;
                foreach (var di in pathDetails)
                {
                    Console.WriteLine("{0}:{1}",
                        di.Item1.ToString().PadLeft(indexWidth),
                        DisplayBuilder.BuildDisplayText(di.Item2, arguments)
                        );
                }

                return 0;
            }
            catch (CommandLineArgumentException ex)
            {
                parser.WriteUsageToConsole(options);
                Console.WriteLine(ex.Message);
                return 1;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return 2;
            }
        }

        private static void EnrichPathDetails(IList<Tuple<int, PathDetails>> pathDetails)
        {
            var duplicatePathIndexes = pathDetails
                .GroupBy(pd => pd.Item2.DirectoryInfo.FullName)
                .ToDictionary(g => g.Key, g => g.Min(x => x.Item1));

            pathDetails
                .Where(pd => duplicatePathIndexes.ContainsKey(pd.Item2.DirectoryInfo.FullName))
                .Where(pd => duplicatePathIndexes[pd.Item2.DirectoryInfo.FullName] != pd.Item1)
                .ToList()
                .ForEach(pd => pd.Item2.IsDuplicate = true);
        }

        private static void ShowAppHeader(IAppHeader appHeader)
        {
            var lines = appHeader.GetOutputLines()
                .ToList();

            if (lines.Any())
            {
                lines.Add(string.Empty);

                lines.ForEach(Console.WriteLine);
            }
        }

        private static Dictionary<int, string> SplitPaths(string pathText)
        {
            var index = 0;
            var paths = (pathText ?? string.Empty).Split(Path.PathSeparator, StringSplitOptions.RemoveEmptyEntries)
                .ToDictionary(_ => ++index, x => x);

            return paths;
        }

        private static async Task<IList<Tuple<int, PathDetails>>> BuildPathDetails(Dictionary<int, string> paths)
        {
            var tasks = paths
                    .Select(p => Task.Run(() => new Tuple<int, PathDetails>(p.Key, new PathDetails(p.Value, new DirectoryInfo(p.Value)))))
                ;

            var results = (await Task.WhenAll(tasks))
                .OrderBy(x => x.Item1)
                .ToArray();

            return results;
        }

        private static string GetEnvironmentVariableValue(Arguments arguments)
        {
            var value = CoalesceNullOrEmpty(
                () => Environment.GetEnvironmentVariable(arguments.EnvironmentVariableName)
            );

            return value;
        }

        private static string CoalesceNullOrEmpty(params Func<string>[] functions)
        {
            var value = functions
                .FirstOrDefault(f => !string.IsNullOrEmpty(f.Invoke()))
                ?.Invoke();

            return value;
        }
    }
}
