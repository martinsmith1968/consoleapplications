﻿using Ookii.CommandLine;
using PathList.Interfaces;

namespace PathList
{
    public class Arguments : IPathDisplayOptions
    {
        [Alias("v")]
        [CommandLineArgument(IsRequired = false, DefaultValue = "PATH")]
        public string EnvironmentVariableName { get; set; }

        [Alias("hm")]
        [CommandLineArgument(IsRequired = false, DefaultValue = IPathDisplayOptions.DefaultHighlightMissingFolders)]
        public bool HighlightMissingFolders { get; set; }

        [Alias("hd")]
        [CommandLineArgument(IsRequired = false, DefaultValue = IPathDisplayOptions.DefaultHighlightMissingFolders)]
        public bool HighlightDuplicatePaths { get; set; }

        [Alias("sfp")]
        [CommandLineArgument(IsRequired = false, DefaultValue = IPathDisplayOptions.DefaultShowFullPathName)]
        public bool ShowFullPathName { get; set; }
    }
}
