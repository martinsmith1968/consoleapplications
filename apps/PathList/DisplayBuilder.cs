﻿using PathList.Interfaces;

namespace PathList
{
    public class DisplayBuilder
    {
        public static string BuildDisplayText(PathDetails pathDetails, IPathDisplayOptions displayOptions)
        {
            displayOptions ??= IPathDisplayOptions.Default;

            var directoryNotFound = (!pathDetails.DirectoryInfo.Exists) && displayOptions.HighlightMissingFolders;

            var fullPathDifferent = pathDetails.IsFullPathDifferent;

            var text = $"{(directoryNotFound ? "#" : " ")}{(pathDetails.IsDuplicate ? "!" : " ")}{(fullPathDifferent ? "*" : " ")}{pathDetails.DirectoryInfo.FullName}";

            return text;
        }
    }
}
