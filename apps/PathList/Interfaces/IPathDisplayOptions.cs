﻿namespace PathList.Interfaces
{
    public interface IPathDisplayOptions
    {
        public const bool DefaultHighlightMissingFolders = true;

        public const bool DefaultHighlightDuplicatePaths = true;

        public const bool DefaultShowFullPathName = true;

        bool HighlightMissingFolders { get; }
        bool HighlightDuplicatePaths { get; }
        bool ShowFullPathName { get; }

        public static IPathDisplayOptions Default => new DefaultDisplayOptions();
    }

    internal class DefaultDisplayOptions : IPathDisplayOptions
    {
        public bool HighlightMissingFolders => IPathDisplayOptions.DefaultHighlightMissingFolders;
        public bool HighlightDuplicatePaths => IPathDisplayOptions.DefaultHighlightDuplicatePaths;
        public bool ShowFullPathName => IPathDisplayOptions.DefaultShowFullPathName;
    }
}
