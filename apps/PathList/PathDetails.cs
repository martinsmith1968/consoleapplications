﻿using System;
using System.IO;

namespace PathList
{
    public class PathDetails
    {
        public string Name { get; }

        public DirectoryInfo DirectoryInfo { get; }

        public bool IsDuplicate { get; set; }

        public bool IsFullPathDifferent => !string.Equals(Name, DirectoryInfo.FullName, StringComparison.CurrentCultureIgnoreCase);

        public PathDetails(string name, DirectoryInfo directoryInfo)
        {
            Name          = name;
            DirectoryInfo = directoryInfo;
        }
    }
}
