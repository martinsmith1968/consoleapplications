﻿using System.Linq;
using System.Reflection;
using AutoFixture;
using Shouldly;
using Xunit;

namespace ConsoleApplications.Common.Tests
{
    public class AppHeaderTests
    {
        protected static readonly Fixture AutoFixture = new Fixture();

        public class Constructor
        {
            [Fact]
            public void Default_uses_entry_assembly()
            {
                // Arrange
                var assembly = Assembly.GetEntryAssembly();

                // Act
                var result = new AppHeader();

                // Assert
                result.GetOutputLines().ShouldNotBeNull();
                result.GetOutputLines().Length.ShouldBeGreaterThan(0);

                result.GetOutputLines().First().ShouldStartWith(assembly.GetName().Name);
            }
        }
    }
}
